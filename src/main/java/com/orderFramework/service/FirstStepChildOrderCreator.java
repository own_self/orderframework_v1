package com.orderFramework.service;

import com.orderFramework.models.ChildOrder;
import com.orderFramework.models.OrderExecutionContext;

public interface FirstStepChildOrderCreator {

    ChildOrder createFirstStepChildOrder(OrderExecutionContext orderExecutionContext);

}
