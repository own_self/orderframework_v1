package com.orderFramework.service.impl;

import com.orderFramework.models.*;
import com.orderFramework.service.OrderUpdateHelper;
import com.orderFramework.service.dao.ChildInfoDao;
import com.orderFramework.service.dao.OrderInfoDao;
import org.springframework.beans.factory.annotation.Autowired;

public class DefaultOrderUpdateHelper implements OrderUpdateHelper {

    @Autowired
    private OrderInfoDao orderInfoDao;

    @Autowired
    private ChildInfoDao childInfoDao;

    @Override
    public void markOrderFirstStepSuccess(OrderExecutionContext orderExecutionContext) {

        ChildOrder firstStepPendingChild = orderExecutionContext.getFirstStepPendingChild();
        firstStepPendingChild.setStatus(SingleStepStatus.SUCCESS);
        childInfoDao.updateChildOrder(firstStepPendingChild);

    }

    @Override
    public void markOrderFirstStepPending(OrderExecutionContext orderExecutionContext) {

        OrderInfo orderInfo = orderExecutionContext.getOrder().getOrderInfo();
        orderInfo.setProcessStatus(OrderProcessStatus.INBOUND_PENDING.toString());
        orderInfoDao.updateOrderInfo(orderInfo);
    }

    @Override
    public void markOrderFirstStepFailed(OrderExecutionContext orderExecutionContext) {

        ChildOrder firstStepPendingChild = orderExecutionContext.getFirstStepPendingChild();
        firstStepPendingChild.setStatus(SingleStepStatus.FAILED);
        childInfoDao.updateChildOrder(firstStepPendingChild);

    }

    @Override
    public void markOrderSecondStepSuccess(OrderExecutionContext orderExecutionContext) {
        ChildOrder secondStepPendingChild = orderExecutionContext.getSecondStepPendingChild();
        secondStepPendingChild.setStatus(SingleStepStatus.SUCCESS);
        childInfoDao.updateChildOrder(secondStepPendingChild);

        OrderInfo orderInfo = orderExecutionContext.getOrder().getOrderInfo();
        orderInfo.setProcessStatus(OrderProcessStatus.SUCCESS.toString());
        orderInfoDao.updateOrderInfo(orderInfo);
    }

    @Override
    public void markOrderSecondStepPending(OrderExecutionContext orderExecutionContext) {
        ChildOrder secondStepPendingChild = orderExecutionContext.getSecondStepPendingChild();
        secondStepPendingChild.setStatus(SingleStepStatus.PENDING);
        childInfoDao.updateChildOrder(secondStepPendingChild);
    }

    @Override
    public void markOrderSecondStepFailed(OrderExecutionContext orderExecutionContext) {

    }

    @Override
    public void markOrderSuccess(OrderExecutionContext orderExecutionContext) {

    }

    @Override
    public void markOrderFailed(OrderExecutionContext orderExecutionContext) {

    }

    @Override
    public void markOrderCancelled(OrderExecutionContext orderExecutionContext) {

    }

    @Override
    public void markOrderExpired(OrderExecutionContext orderExecutionContext) {

    }

    @Override
    public void markOrderPending(OrderExecutionContext orderExecutionContext) {

    }
}
