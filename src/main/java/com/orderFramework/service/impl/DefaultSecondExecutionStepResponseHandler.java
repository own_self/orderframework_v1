package com.orderFramework.service.impl;

import com.orderFramework.models.*;
import com.orderFramework.service.OrderExecutor;
import com.orderFramework.service.OrderUpdateHelper;
import com.orderFramework.service.SecondStepExecutionResponseHandler;
import com.orderFramework.service.dao.ChildInfoDao;
import org.springframework.beans.factory.annotation.Autowired;

public class DefaultSecondExecutionStepResponseHandler implements SecondStepExecutionResponseHandler {

    @Autowired
    private OrderUpdateHelper orderUpdateHelper;

    @Autowired
    private ChildInfoDao childInfoDao;

    @Override
    public SecondStepExecutionResult handleSecondStepResponse(OrderExecutionContext orderExecutionContext, SingleStepResponse singleStepResponse) {

        SecondStepExecutionResult secondStepExecutionResult = new SecondStepExecutionResult();
        secondStepExecutionResult.setTerminal(false);

        OrderExecutor orderExecutor = orderExecutionContext.getOrderExecutor();
        if (singleStepResponse.isTimeout() || singleStepResponse.getHttpStatus() != 200) {
            orderUpdateHelper.markOrderSecondStepPending(orderExecutionContext);
        } else {
            switch (singleStepResponse.getSingleStepStatus()) {
                case SUCCESS:
                    orderUpdateHelper.markOrderSecondStepSuccess(orderExecutionContext);
                    orderUpdateHelper.markOrderSuccess(orderExecutionContext);
                    secondStepExecutionResult.setTerminal(true);
                    break;
                case FAILED:
                    orderUpdateHelper.markOrderSecondStepFailed(orderExecutionContext);
                    if(orderExecutor.getRetryHandler().isSecondStepRetriable(orderExecutionContext)) {
                        if(orderExecutor.getRetryHandler().isSecondStepAutoRetry(orderExecutionContext)) {
                            ChildOrder pendingChild = new ChildOrder();
                            pendingChild.setPaytmOrderId(orderExecutionContext.getOrder().getPaytmOrderId());
                            pendingChild.setStatus(SingleStepStatus.PENDING);
                            pendingChild.setUniqueReferenceId(null);
                            childInfoDao.insertChildOrder(pendingChild);
                            secondStepExecutionResult.setTerminal(false);
                        }
                    } else {
                        orderUpdateHelper.markOrderFailed(orderExecutionContext);
                        secondStepExecutionResult.setTerminal(true);
                    }
                    break;
                case PENDING:
                    orderUpdateHelper.markOrderSecondStepPending(orderExecutionContext);
                    break;
            }
        }

        secondStepExecutionResult.setProceed(true);
        return secondStepExecutionResult;
    }
}
