package com.orderFramework.service.impl;

import com.orderFramework.models.*;
import com.orderFramework.service.FirstStepResponseHandler;
import com.orderFramework.service.OrderUpdateHelper;
import com.orderFramework.service.dao.ChildInfoDao;
import com.orderFramework.service.dao.OrderInfoDao;
import org.springframework.beans.factory.annotation.Autowired;

public class DefaultFirstStepResponseHandler implements FirstStepResponseHandler {

    @Autowired
    private OrderUpdateHelper orderUpdateHelper;

    @Autowired
    private OrderInfoDao orderInfoDao;

    @Autowired
    private ChildInfoDao childInfoDao;

    @Override
    public FirstStepExecutionResult handleResponse(OrderExecutionContext orderExecutionContext, SingleStepResponse singleStepResponse) {
        FirstStepExecutionResult firstStepExecutionResult = new FirstStepExecutionResult();

        if(singleStepResponse.isTimeout()) {
            orderUpdateHelper.markOrderFirstStepPending(orderExecutionContext);
        } else if (singleStepResponse.getHttpStatus() != 200) {
            orderUpdateHelper.markOrderFirstStepPending(orderExecutionContext);
        } else {
            switch (singleStepResponse.getSingleStepStatus()) {
                case SUCCESS:
                    orderUpdateHelper.markOrderFirstStepSuccess(orderExecutionContext);
                    break;
                case PENDING:
                    orderUpdateHelper.markOrderFirstStepPending(orderExecutionContext);
                    break;
                case FAILED:
                    boolean firstStepRetriable = orderExecutionContext.getOrderExecutor().getRetryHandler().isFirstStepRetriable(orderExecutionContext);
                    orderUpdateHelper.markOrderFirstStepFailed(orderExecutionContext);
                    if (firstStepRetriable) {
                        if (orderExecutionContext.getOrderExecutor().getRetryHandler().isFirstStepAutoRetry(orderExecutionContext)) {
                            ChildOrder pendingChildOrder = new ChildOrder();
                            pendingChildOrder.setPaytmOrderId(orderExecutionContext.getOrder().getPaytmOrderId());
                            pendingChildOrder.setStatus(SingleStepStatus.PENDING);
                            pendingChildOrder.setUniqueReferenceId(null);
                            childInfoDao.insertChildOrder(pendingChildOrder);
                        }
                    }
            }
        }

        firstStepExecutionResult.setProceed(true);
        return firstStepExecutionResult;
    }
}
