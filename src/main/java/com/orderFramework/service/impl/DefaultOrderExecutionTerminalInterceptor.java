package com.orderFramework.service.impl;

import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.service.OrderExecutionTerminalInterceptor;
import com.orderFramework.service.OrderExecutor;

public class DefaultOrderExecutionTerminalInterceptor implements OrderExecutionTerminalInterceptor {

    @Override
    public void interceptOrderExecutionTerminal(OrderExecutionContext orderExecutionContext) {
        OrderExecutor orderExecutor = orderExecutionContext.getOrderExecutor();
        orderExecutionContext.getOrder().getProcessStatus();
    }
}
