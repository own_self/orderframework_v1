package com.orderFramework.service.impl;


import com.orderFramework.models.FirstStepStatusCheckResult;
import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.StatusCheckResponse;
import com.orderFramework.service.OrderExecutor;
import com.orderFramework.service.FirstStepStatusCheckResponseHandler;
import com.orderFramework.service.OrderUpdateHelper;
import com.orderFramework.service.retryHandler.RetryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultFirstStepStatusCheckResponseHandler implements FirstStepStatusCheckResponseHandler {

    @Autowired
    private OrderUpdateHelper orderUpdateHelper;

    @Override
    public FirstStepStatusCheckResult handleStatusCheckResponse(OrderExecutionContext orderExecutionContext, StatusCheckResponse statusCheckResponse) {

        FirstStepStatusCheckResult firstStepStatusCheckResult = new FirstStepStatusCheckResult();

        OrderExecutor orderExecutor = orderExecutionContext.getOrderExecutor();
        RetryHandler retryHandler = orderExecutor.getRetryHandler();
        if(retryHandler.isFirstStepCheckStatusRetry(orderExecutionContext)) {
            if (statusCheckResponse.isTimeout() || statusCheckResponse.getHttpStatus() != 200) {
                retryHandler.handleFirstStepCheckStatusRetry(orderExecutionContext);
            } else {
                switch (statusCheckResponse.getStatus()) {
                    case SUCCESS:
                        orderUpdateHelper.markOrderFirstStepSuccess(orderExecutionContext);
                        break;
                    case FAILED:
                        orderUpdateHelper.markOrderFirstStepFailed(orderExecutionContext);
                        break;
                    case PENDING:
                        orderUpdateHelper.markOrderFirstStepPending(orderExecutionContext);
                        break;
                    case EXPIRED:
                        orderUpdateHelper.markOrderExpired(orderExecutionContext);
                        break;
                    case CANCELLED:
                        orderUpdateHelper.markOrderCancelled(orderExecutionContext);
                        break;
                    default:
                }
            }
        }
        return null;
    }
}
