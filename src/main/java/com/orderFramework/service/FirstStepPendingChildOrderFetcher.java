package com.orderFramework.service;

import com.orderFramework.models.ChildOrder;

import java.util.List;

public interface FirstStepPendingChildOrderFetcher {

    ChildOrder fetchPendingChildOrder(List<ChildOrder> childOrders);

}
