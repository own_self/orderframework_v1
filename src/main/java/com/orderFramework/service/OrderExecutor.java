package com.orderFramework.service;

import com.orderFramework.models.Webhook;
import com.orderFramework.models.config.OrderExecutionConfig;
import com.orderFramework.service.retryHandler.RetryHandler;

import java.util.Optional;

public class OrderExecutor {

    private OrderGetter orderGetter;

    private OrderExecutionConfig orderExecutionConfig;
    private StepExecutor firstStepExecutor;
    private StepExecutor secondStepExecutor;
    private RetryHandler retryHandler;
    private FirstStepStatusCheckResponseHandler firstStepStatusCheckResponseHandler;
    private FirstStepResponseHandler firstStepResponseHandler;
    private SecondStepCheckStatusResponseHandler secondStepCheckStatusResponseHandler;
    private SecondStepExecutionResponseHandler secondStepExecutionResponseHandler;
    private OrderExecutionTerminalInterceptor orderExecutionTerminalInterceptor;
    private OrderExecutionPostExecutionHandler orderExecutionPostExecutionHandler;

    private FirstStepPendingChildOrderFetcher firstStepPendingChildOrderFetcher;
    private SecondStepPendingChildOrderFetcher secondStepPendingChildOrderFetcher;
    private FirstStepChildOrderCreator firstStepChildOrderCreator;
    private SecondStepChildOrderCreator secondStepChildOrderCreator;

    private Optional<Webhook> successWebhookOptional;
    private Optional<Webhook> failureWebhookOptional;
    private Optional<Webhook> expiredWebhookOptional;
    private Optional<Webhook> cancelledWebhookOptional;


    public OrderExecutionConfig getOrderExecutionConfig() {
        return orderExecutionConfig;
    }

    public void setOrderExecutionConfig(OrderExecutionConfig orderExecutionConfig) {
        this.orderExecutionConfig = orderExecutionConfig;
    }

    public StepExecutor getFirstStepExecutor() {
        return firstStepExecutor;
    }

    public void setFirstStepExecutor(StepExecutor firstStepExecutor) {
        this.firstStepExecutor = firstStepExecutor;
    }

    public StepExecutor getSecondStepExecutor() {
        return secondStepExecutor;
    }

    public void setSecondStepExecutor(StepExecutor secondStepExecutor) {
        this.secondStepExecutor = secondStepExecutor;
    }

    public RetryHandler getRetryHandler() {
        return retryHandler;
    }

    public void setRetryHandler(RetryHandler retryHandler) {
        this.retryHandler = retryHandler;
    }

    public FirstStepStatusCheckResponseHandler getFirstStepStatusCheckResponseHandler() {
        return firstStepStatusCheckResponseHandler;
    }

    public void setFirstStepStatusCheckResponseHandler(FirstStepStatusCheckResponseHandler firstStepStatusCheckResponseHandler) {
        this.firstStepStatusCheckResponseHandler = firstStepStatusCheckResponseHandler;
    }

    public FirstStepResponseHandler getFirstStepResponseHandler() {
        return firstStepResponseHandler;
    }

    public void setFirstStepResponseHandler(FirstStepResponseHandler firstStepResponseHandler) {
        this.firstStepResponseHandler = firstStepResponseHandler;
    }

    public SecondStepCheckStatusResponseHandler getSecondStepCheckStatusResponseHandler() {
        return secondStepCheckStatusResponseHandler;
    }

    public void setSecondStepCheckStatusResponseHandler(SecondStepCheckStatusResponseHandler secondStepCheckStatusResponseHandler) {
        this.secondStepCheckStatusResponseHandler = secondStepCheckStatusResponseHandler;
    }

    public SecondStepExecutionResponseHandler getSecondStepExecutionResponseHandler() {
        return secondStepExecutionResponseHandler;
    }

    public void setSecondStepExecutionResponseHandler(SecondStepExecutionResponseHandler secondStepExecutionResponseHandler) {
        this.secondStepExecutionResponseHandler = secondStepExecutionResponseHandler;
    }

    public OrderExecutionTerminalInterceptor getOrderExecutionTerminalInterceptor() {
        return orderExecutionTerminalInterceptor;
    }

    public void setOrderExecutionTerminalInterceptor(OrderExecutionTerminalInterceptor orderExecutionTerminalInterceptor) {
        this.orderExecutionTerminalInterceptor = orderExecutionTerminalInterceptor;
    }

    public OrderExecutionPostExecutionHandler getOrderExecutionPostExecutionHandler() {
        return orderExecutionPostExecutionHandler;
    }

    public void setOrderExecutionPostExecutionHandler(OrderExecutionPostExecutionHandler orderExecutionPostExecutionHandler) {
        this.orderExecutionPostExecutionHandler = orderExecutionPostExecutionHandler;
    }

    public FirstStepPendingChildOrderFetcher getFirstStepPendingChildOrderFetcher() {
        return firstStepPendingChildOrderFetcher;
    }

    public void setFirstStepPendingChildOrderFetcher(FirstStepPendingChildOrderFetcher firstStepPendingChildOrderFetcher) {
        this.firstStepPendingChildOrderFetcher = firstStepPendingChildOrderFetcher;
    }

    public SecondStepPendingChildOrderFetcher getSecondStepPendingChildOrderFetcher() {
        return secondStepPendingChildOrderFetcher;
    }

    public void setSecondStepPendingChildOrderFetcher(SecondStepPendingChildOrderFetcher secondStepPendingChildOrderFetcher) {
        this.secondStepPendingChildOrderFetcher = secondStepPendingChildOrderFetcher;
    }

    public FirstStepChildOrderCreator getFirstStepChildOrderCreator() {
        return firstStepChildOrderCreator;
    }

    public void setFirstStepChildOrderCreator(FirstStepChildOrderCreator firstStepChildOrderCreator) {
        this.firstStepChildOrderCreator = firstStepChildOrderCreator;
    }

    public SecondStepChildOrderCreator getSecondStepChildOrderCreator() {
        return secondStepChildOrderCreator;
    }

    public void setSecondStepChildOrderCreator(SecondStepChildOrderCreator secondStepChildOrderCreator) {
        this.secondStepChildOrderCreator = secondStepChildOrderCreator;
    }

    public OrderGetter getOrderGetter() {
        return orderGetter;
    }

    public void setOrderGetter(OrderGetter orderGetter) {
        this.orderGetter = orderGetter;
    }

    public Optional<Webhook> getSuccessWebhookOptional() {
        return successWebhookOptional;
    }

    public void setSuccessWebhookOptional(Optional<Webhook> successWebhookOptional) {
        this.successWebhookOptional = successWebhookOptional;
    }

    public Optional<Webhook> getFailureWebhookOptional() {
        return failureWebhookOptional;
    }

    public void setFailureWebhookOptional(Optional<Webhook> failureWebhookOptional) {
        this.failureWebhookOptional = failureWebhookOptional;
    }

    public Optional<Webhook> getExpiredWebhookOptional() {
        return expiredWebhookOptional;
    }

    public void setExpiredWebhookOptional(Optional<Webhook> expiredWebhookOptional) {
        this.expiredWebhookOptional = expiredWebhookOptional;
    }

    public Optional<Webhook> getCancelledWebhookOptional() {
        return cancelledWebhookOptional;
    }

    public void setCancelledWebhookOptional(Optional<Webhook> cancelledWebhookOptional) {
        this.cancelledWebhookOptional = cancelledWebhookOptional;
    }
}
