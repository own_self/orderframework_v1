package com.orderFramework.service.retryHandler;

import com.orderFramework.models.OrderExecutionContext;

public interface RetryHandler {

    boolean isFirstStepCheckStatusRetry(OrderExecutionContext orderExecutionContext);
    void handleFirstStepCheckStatusRetry(OrderExecutionContext orderExecutionContext);

    void handleSecondStepCheckStatusRetry(OrderExecutionContext orderExecutionContext);

    boolean isFirstStepRetriable(OrderExecutionContext orderExecutionContext);
    boolean isFirstStepAutoRetry(OrderExecutionContext orderExecutionContext);
    boolean isSecondStepRetriable(OrderExecutionContext orderExecutionContext);
    boolean isSecondStepAutoRetry(OrderExecutionContext orderExecutionContext);
}
