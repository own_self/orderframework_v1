package com.orderFramework.service.retryHandler;

import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.config.OrderExecutionConfig;

public class DefaultRetryHandler implements RetryHandler {

    @Override
    public boolean isFirstStepCheckStatusRetry(OrderExecutionContext orderExecutionContext) {
        return orderExecutionContext.getOrder().getFirstStepRetryCount() >= orderExecutionContext.getOrderExecutor().getOrderExecutionConfig().getFirstStepCheckStatusPendingMaxRetry();
    }

    @Override
    public void handleFirstStepCheckStatusRetry(OrderExecutionContext orderExecutionContext) {
        OrderExecutionConfig orderExecutionConfig = orderExecutionContext.getOrderExecutor().getOrderExecutionConfig();

    }

    @Override
    public void handleSecondStepCheckStatusRetry(OrderExecutionContext orderExecutionContext) {

    }

    @Override
    public boolean isFirstStepRetriable(OrderExecutionContext orderExecutionContext) {
        return false;
    }

    @Override
    public boolean isFirstStepAutoRetry(OrderExecutionContext orderExecutionContext) {
        return true;
    }

    @Override
    public boolean isSecondStepRetriable(OrderExecutionContext orderExecutionContext) {
        return true;
    }

    @Override
    public boolean isSecondStepAutoRetry(OrderExecutionContext orderExecutionContext) {
        return true;
    }
}
