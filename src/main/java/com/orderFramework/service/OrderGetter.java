package com.orderFramework.service;

import com.orderFramework.models.Order;

public interface OrderGetter {

    Order fetchOrder(int paytmOrderId);

}
