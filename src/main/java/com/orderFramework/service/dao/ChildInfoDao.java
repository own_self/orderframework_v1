package com.orderFramework.service.dao;

import com.orderFramework.models.ChildOrder;
import com.orderFramework.models.OrderInfo;

import java.util.List;

public interface ChildInfoDao {

    List<ChildOrder> fetchChildOrders(String paytmOrderId);
    ChildOrder fetchChildOrder(String childOrderId);

    ChildOrder insertChildOrder(ChildOrder childOrder);
    void updateChildOrder(ChildOrder childOrder);

}
