package com.orderFramework.service.dao;

import com.orderFramework.models.ChildOrder;

import java.util.List;

public class DefaultChildInfoDao implements ChildInfoDao {
    @Override
    public List<ChildOrder> fetchChildOrders(String paytmOrderId) {
        return null;
    }

    @Override
    public ChildOrder fetchChildOrder(String childOrderId) {
        return null;
    }

    @Override
    public ChildOrder insertChildOrder(ChildOrder childOrder) {
        return null;
    }

    @Override
    public void updateChildOrder(ChildOrder childOrder) {

    }
}
