package com.orderFramework.service.dao;

import com.orderFramework.models.OrderInfo;

public interface OrderInfoDao {

    OrderInfo fetchOrderInfoByPaytmOrderId(String paytmOrderId);
    OrderInfo fetchOrderInfoByReferenceId(String uniqueReferenceId);

    OrderInfo insertOrderInfo(OrderInfo orderInfo);
    void updateOrderInfo(OrderInfo orderInfo);

}
