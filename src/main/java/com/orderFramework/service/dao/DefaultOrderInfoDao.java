package com.orderFramework.service.dao;

import com.orderFramework.models.OrderInfo;

public class DefaultOrderInfoDao implements OrderInfoDao {


    @Override
    public OrderInfo fetchOrderInfoByPaytmOrderId(String paytmOrderId) {
        return null;
    }

    @Override
    public OrderInfo fetchOrderInfoByReferenceId(String uniqueReferenceId) {
        return null;
    }

    @Override
    public OrderInfo insertOrderInfo(OrderInfo orderInfo) {
        return null;
    }

    @Override
    public void updateOrderInfo(OrderInfo orderInfo) {

    }
}
