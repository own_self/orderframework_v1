package com.orderFramework.service;

import com.orderFramework.models.Order;
import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.OrderProcessStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface OrderUpdateHelper {

    void markOrderFirstStepSuccess(OrderExecutionContext orderExecutionContext);
    void markOrderFirstStepPending(OrderExecutionContext orderExecutionContext);
    void markOrderFirstStepFailed(OrderExecutionContext orderExecutionContext);

    void markOrderSecondStepSuccess(OrderExecutionContext orderExecutionContext);
    void markOrderSecondStepPending(OrderExecutionContext orderExecutionContext);
    void markOrderSecondStepFailed(OrderExecutionContext orderExecutionContext);

    void markOrderSuccess(OrderExecutionContext orderExecutionContext);
    void markOrderFailed(OrderExecutionContext orderExecutionContext);
    void markOrderCancelled(OrderExecutionContext orderExecutionContext);
    void markOrderExpired(OrderExecutionContext orderExecutionContext);
    void markOrderPending(OrderExecutionContext orderExecutionContext);



}
