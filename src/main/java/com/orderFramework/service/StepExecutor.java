package com.orderFramework.service;

import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.RefundResponse;
import com.orderFramework.models.SingleStepResponse;
import com.orderFramework.models.StatusCheckResponse;

public interface StepExecutor {

    void preExecute(OrderExecutionContext orderExecutionContext);
    SingleStepResponse executeStep(OrderExecutionContext orderExecutionContext);
    StatusCheckResponse checkStatus(OrderExecutionContext orderExecutionContext);
    RefundResponse initiateRefund(OrderExecutionContext orderExecutionContext);
    void postExecute(OrderExecutionContext orderExecutionContext);

}
