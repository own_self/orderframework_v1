package com.orderFramework.service;

import com.orderFramework.models.*;

public interface OrderExecutionContextMapper {

    OrderType mapOrderType(Order order);
    OrderExecutionType mapExecutionType(Order order);
    OrderBusinessContext mapOrderBusinessContext(Order order);

}
