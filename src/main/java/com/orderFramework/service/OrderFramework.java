package com.orderFramework.service;

import com.orderFramework.models.*;
import com.orderFramework.service.retryHandler.RetryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrderFramework {

    @Autowired
    private OrderGetter orderGetter;

    @Autowired
    private OrderExecutionContextMapper orderExecutionContextMapper;

    @Autowired
    private OrderExecutorMapper orderExecutorMapper;

    public void triggerFirstStepExecution(int paytmOrderId) {
        OrderExecutionContext orderExecutionContext = initializeOrderExecutionContext(paytmOrderId);
        executeFirstStep(orderExecutionContext);
    }

    public void triggerSecondStepExecution(int paytmOrderId) {
        OrderExecutionContext orderExecutionContext = initializeOrderExecutionContext(paytmOrderId);
        executeSecondStep(orderExecutionContext);
    }

    public void submit(int paytmOrderId) {

        OrderExecutionContext orderExecutionContext = initializeOrderExecutionContext(paytmOrderId);
        Order order = orderExecutionContext.getOrder();

        switch (order.getProcessStatus()) {
            case PROCESSING:
            case INBOUND_PENDING:
            case INBOUND_CHILD_FAILED:
                executeFirstStep(orderExecutionContext);
                break;
            case OUTBOUND_PENDING:
            case OUTBOUND_CHILD_FAILED:
                executeSecondStep(orderExecutionContext);
                break;
        }
    }

    private void executeFirstStep(OrderExecutionContext orderExecutionContext) {
        OrderExecutor orderExecutor = orderExecutionContext.getOrderExecutor();

        boolean isStatusCheckRequired = true;

        ChildOrder pendingChild = orderExecutor.getFirstStepPendingChildOrderFetcher()
                .fetchPendingChildOrder(orderExecutionContext.getOrder().getFirstStepChildOrders());
        if(pendingChild == null) {
            pendingChild = orderExecutor.getFirstStepChildOrderCreator().createFirstStepChildOrder(orderExecutionContext);
            orderExecutionContext.getOrder().getFirstStepChildOrders().add(pendingChild);
            isStatusCheckRequired = false;
        }

        orderExecutionContext.setFirstStepPendingChild(pendingChild);
        FirstStepStatusCheckResult firstStepStatusCheckResult = null;
        if (isStatusCheckRequired) {
            StatusCheckResponse statusCheckResponse = orderExecutor.getFirstStepExecutor().checkStatus(orderExecutionContext);
            firstStepStatusCheckResult = orderExecutor.getFirstStepStatusCheckResponseHandler()
                    .handleStatusCheckResponse(orderExecutionContext, statusCheckResponse);
        }
        if(!isStatusCheckRequired || firstStepStatusCheckResult.isProceed()) {
            SingleStepResponse singleStepResponse = orderExecutor.getFirstStepExecutor().executeStep(orderExecutionContext);
            orderExecutionContext.setFirstStepResponse(singleStepResponse);
            FirstStepExecutionResult firstStepExecutionResult = orderExecutor.getFirstStepResponseHandler()
                    .handleResponse(orderExecutionContext, singleStepResponse);
            orderExecutor.getFirstStepExecutor().postExecute(orderExecutionContext);
            if (firstStepExecutionResult.isProceed()) {
                executeSecondStep(orderExecutionContext);
            }
        }
    }

    private void executeSecondStep(OrderExecutionContext orderExecutionContext) {
        OrderExecutor orderExecutor = orderExecutionContext.getOrderExecutor();

        boolean isStatusCheckRequired = true;

        ChildOrder pendingChild = orderExecutor.getSecondStepPendingChildOrderFetcher().fetchPendingChild(orderExecutionContext);

        if(pendingChild == null) {
            pendingChild = orderExecutor.getSecondStepChildOrderCreator().createChildOrder(orderExecutionContext);
            orderExecutionContext.getOrder().getSecondStepChildOrders().add(pendingChild);
            isStatusCheckRequired = false;
        }

        orderExecutionContext.setSecondStepPendingChild(pendingChild);

        SecondStepStatusCheckResult secondStepStatusCheckResult = null;

        if(isStatusCheckRequired) {
            StatusCheckResponse statusCheckResponse = orderExecutor.getSecondStepExecutor().checkStatus(orderExecutionContext);
            secondStepStatusCheckResult = orderExecutor.getSecondStepCheckStatusResponseHandler()
                    .handleSecondStepStatusCheckResponse(statusCheckResponse);
        }
        if(!isStatusCheckRequired || secondStepStatusCheckResult.isProceed()) {
            SingleStepResponse singleStepResponse = orderExecutor.getSecondStepExecutor().executeStep(orderExecutionContext);
            orderExecutionContext.setSecondStepResponse(singleStepResponse);
            SecondStepExecutionResult secondStepExecutionResult = orderExecutor.getSecondStepExecutionResponseHandler()
                    .handleSecondStepResponse(orderExecutionContext, singleStepResponse);
            if(secondStepExecutionResult.isProceed()) {
                if(secondStepExecutionResult.isTerminal()) {
                    orderExecutor.getOrderExecutionTerminalInterceptor().interceptOrderExecutionTerminal(orderExecutionContext);
                }
            }
        }
    }

    private OrderExecutionContext initializeOrderExecutionContext(int paytmOrderId) {
        Order order = orderGetter.fetchOrder(paytmOrderId);

        OrderExecutionType orderExecutionType = orderExecutionContextMapper.mapExecutionType(order);
        OrderBusinessContext orderBusinessContext = orderExecutionContextMapper.mapOrderBusinessContext(order);

        OrderExecutionContext orderExecutionContext = new OrderExecutionContext();
        orderExecutionContext.setOrder(order);
        orderExecutionContext.setOrderExecutionType(orderExecutionType);
        orderExecutionContext.setOrderBusinessContext(orderBusinessContext);

        OrderExecutor orderExecutor = orderExecutorMapper.getOrderExecutor(orderExecutionContext);
        orderExecutionContext.setOrderExecutor(orderExecutor);

        return orderExecutionContext;
    }

}
