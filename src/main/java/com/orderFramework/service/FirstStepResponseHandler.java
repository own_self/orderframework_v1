package com.orderFramework.service;

import com.orderFramework.models.FirstStepExecutionResult;
import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.SingleStepResponse;

public interface FirstStepResponseHandler {

    FirstStepExecutionResult handleResponse(OrderExecutionContext orderExecutionContext, SingleStepResponse singleStepResponse);

}
