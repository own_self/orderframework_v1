package com.orderFramework.service;

import com.orderFramework.models.OrderExecutionContext;

public interface OrderExecutionTerminalInterceptor {

    void interceptOrderExecutionTerminal(OrderExecutionContext orderExecutionContext);

}
