package com.orderFramework.service;

import com.orderFramework.models.ChildOrder;
import com.orderFramework.models.OrderExecutionContext;

public interface SecondStepPendingChildOrderFetcher {

    ChildOrder fetchPendingChild(OrderExecutionContext orderExecutionContext);

}
