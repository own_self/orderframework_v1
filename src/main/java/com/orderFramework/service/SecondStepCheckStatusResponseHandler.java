package com.orderFramework.service;

import com.orderFramework.models.SecondStepStatusCheckResult;
import com.orderFramework.models.StatusCheckResponse;

public interface SecondStepCheckStatusResponseHandler {

    SecondStepStatusCheckResult handleSecondStepStatusCheckResponse(StatusCheckResponse statusCheckResponse);

}
