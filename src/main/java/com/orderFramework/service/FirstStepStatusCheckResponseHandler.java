package com.orderFramework.service;

import com.orderFramework.models.FirstStepStatusCheckResult;
import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.StatusCheckResponse;

public interface FirstStepStatusCheckResponseHandler {

    FirstStepStatusCheckResult handleStatusCheckResponse(OrderExecutionContext orderExecutionContext, StatusCheckResponse statusCheckResponse);

}
