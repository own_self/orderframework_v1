package com.orderFramework.service;

import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.OrderType;

import java.util.HashMap;
import java.util.Map;

public class OrderExecutorMapperImpl implements OrderExecutorMapper {

    private Map<OrderType, OrderExecutor> orderExecutorMap;

    public  OrderExecutorMapperImpl() {
        orderExecutorMap = new HashMap<>();
    }

    @Override
    public OrderExecutor getOrderExecutor(OrderExecutionContext orderExecutionContext) {
        return this.orderExecutorMap.get(orderExecutionContext.getOrder().getOrderType());
    }

    @Override
    public void register(OrderType orderType, OrderExecutor orderExecutor) {
        this.orderExecutorMap.put(orderType, orderExecutor);
    }


}
