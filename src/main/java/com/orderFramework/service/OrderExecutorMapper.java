package com.orderFramework.service;

import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.OrderType;

public interface OrderExecutorMapper {

    OrderExecutor getOrderExecutor(OrderExecutionContext orderExecutionContext);
    void register(OrderType orderType, OrderExecutor orderExecutor);

}
