package com.orderFramework.service;

import com.orderFramework.models.OrderExecutionContext;
import com.orderFramework.models.SecondStepExecutionResult;
import com.orderFramework.models.SingleStepResponse;

public interface SecondStepExecutionResponseHandler {

    SecondStepExecutionResult handleSecondStepResponse(OrderExecutionContext orderExecutionContext, SingleStepResponse singleStepResponse);

}
