package com.orderFramework.service;

import com.orderFramework.models.OrderExecutionContext;

public interface OrderExecutionPostExecutionHandler {

    void postExecute(OrderExecutionContext orderExecutionContext);

}
