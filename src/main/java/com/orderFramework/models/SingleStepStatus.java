package com.orderFramework.models;

public enum SingleStepStatus {

    PENDING, SUCCESS, FAILED, CANCELLED, EXPIRED;

}

