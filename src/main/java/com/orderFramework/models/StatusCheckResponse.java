package com.orderFramework.models;

public class StatusCheckResponse {

    private int httpStatus;
    private SingleStepStatus status;
    private int statusCode;
    private String statusMessage;
    private String downstreamReferenceId;
    private String rawBody;
    private boolean isTimeout;
    private Object meta;

    public String getRawBody() {
        return rawBody;
    }

    public void setRawBody(String rawBody) {
        this.rawBody = rawBody;
    }

    public boolean isTimeout() {
        return isTimeout;
    }

    public void setTimeout(boolean timeout) {
        isTimeout = timeout;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public SingleStepStatus getStatus() {
        return status;
    }

    public void setStatus(SingleStepStatus status) {
        this.status = status;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getDownstreamReferenceId() {
        return downstreamReferenceId;
    }

    public void setDownstreamReferenceId(String downstreamReferenceId) {
        this.downstreamReferenceId = downstreamReferenceId;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }
}
