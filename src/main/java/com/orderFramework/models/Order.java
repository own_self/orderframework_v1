package com.orderFramework.models;

import java.time.LocalDateTime;
import java.util.List;

public class Order {

    private int paytmOrderId;
    private String uniqueReferenceId;
    private String mid;
    private double amount;
    private Currency amount_currency;
    private double commission;
    private Currency commission_currency;
    private OrderProcessStatus processStatus;
    private LocalDateTime retryTime;
    private int firstStepStatusCheckRetryCount;
    private int secondStepStatusCheckRetryCount;
    private int firstStepRetryCount;
    private int secondStepRetryCount;
    private OrderInfo orderInfo;
    private OrderType orderType;
    private List<ChildOrder> firstStepChildOrders;
    private List<ChildOrder> secondStepChildOrders;

    public int getPaytmOrderId() {
        return paytmOrderId;
    }

    public void setPaytmOrderId(int paytmOrderId) {
        this.paytmOrderId = paytmOrderId;
    }

    public String getUniqueReferenceId() {
        return uniqueReferenceId;
    }

    public void setUniqueReferenceId(String uniqueReferenceId) {
        this.uniqueReferenceId = uniqueReferenceId;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getAmount_currency() {
        return amount_currency;
    }

    public void setAmount_currency(Currency amount_currency) {
        this.amount_currency = amount_currency;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public Currency getCommission_currency() {
        return commission_currency;
    }

    public void setCommission_currency(Currency commission_currency) {
        this.commission_currency = commission_currency;
    }

    public OrderProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(OrderProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public LocalDateTime getRetryTime() {
        return retryTime;
    }

    public void setRetryTime(LocalDateTime retryTime) {
        this.retryTime = retryTime;
    }

    public int getFirstStepStatusCheckRetryCount() {
        return firstStepStatusCheckRetryCount;
    }

    public void setFirstStepStatusCheckRetryCount(int firstStepStatusCheckRetryCount) {
        this.firstStepStatusCheckRetryCount = firstStepStatusCheckRetryCount;
    }

    public int getSecondStepStatusCheckRetryCount() {
        return secondStepStatusCheckRetryCount;
    }

    public void setSecondStepStatusCheckRetryCount(int secondStepStatusCheckRetryCount) {
        this.secondStepStatusCheckRetryCount = secondStepStatusCheckRetryCount;
    }

    public int getFirstStepRetryCount() {
        return firstStepRetryCount;
    }

    public void setFirstStepRetryCount(int firstStepRetryCount) {
        this.firstStepRetryCount = firstStepRetryCount;
    }

    public int getSecondStepRetryCount() {
        return secondStepRetryCount;
    }

    public void setSecondStepRetryCount(int secondStepRetryCount) {
        this.secondStepRetryCount = secondStepRetryCount;
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public List<ChildOrder> getFirstStepChildOrders() {
        return firstStepChildOrders;
    }

    public void setFirstStepChildOrders(List<ChildOrder> firstStepChildOrders) {
        this.firstStepChildOrders = firstStepChildOrders;
    }

    public List<ChildOrder> getSecondStepChildOrders() {
        return secondStepChildOrders;
    }

    public void setSecondStepChildOrders(List<ChildOrder> secondStepChildOrders) {
        this.secondStepChildOrders = secondStepChildOrders;
    }
}
