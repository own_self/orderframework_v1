package com.orderFramework.models;

public enum  OrderBusinessContext {

    WALLET_DISBURSEMENT, BANK_DISBURSEMENT, LOYALTY_POINTS, BILL_PAYMENTS, INVOICE_PAYMENT, PENNY_DROP;

}
