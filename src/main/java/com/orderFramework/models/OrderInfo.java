package com.orderFramework.models;

import java.time.LocalDateTime;

public class OrderInfo {

    private int paytmOrderId;
    private String uniqueReferenceId;
    private String mid;
    private double amount;
    private Currency amount_currency;
    private double commission;
    private Currency commission_currency;
    private String processStatus;
    private int retryCount;
    private LocalDateTime nextRetryTime;
    
    private String firstStepStatus;
    private String firstStepStatusCode;
    private String firstStepStatusMessage;

    private String secondStepStatus;
    private String secondStepStatusCode;
    private String secondStepStatusMessage;
    
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;


    public int getPaytmOrderId() {
        return paytmOrderId;
    }

    public void setPaytmOrderId(int paytmOrderId) {
        this.paytmOrderId = paytmOrderId;
    }

    public String getUniqueReferenceId() {
        return uniqueReferenceId;
    }

    public void setUniqueReferenceId(String uniqueReferenceId) {
        this.uniqueReferenceId = uniqueReferenceId;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Currency getAmount_currency() {
        return amount_currency;
    }

    public void setAmount_currency(Currency amount_currency) {
        this.amount_currency = amount_currency;
    }

    public double getCommission() {
        return commission;
    }

    public void setCommission(double commission) {
        this.commission = commission;
    }

    public Currency getCommission_currency() {
        return commission_currency;
    }

    public void setCommission_currency(Currency commission_currency) {
        this.commission_currency = commission_currency;
    }

    public String getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(String processStatus) {
        this.processStatus = processStatus;
    }

    public String getFirstStepStatus() {
        return firstStepStatus;
    }

    public void setFirstStepStatus(String firstStepStatus) {
        this.firstStepStatus = firstStepStatus;
    }

    public String getFirstStepStatusCode() {
        return firstStepStatusCode;
    }

    public void setFirstStepStatusCode(String firstStepStatusCode) {
        this.firstStepStatusCode = firstStepStatusCode;
    }

    public String getFirstStepStatusMessage() {
        return firstStepStatusMessage;
    }

    public void setFirstStepStatusMessage(String firstStepStatusMessage) {
        this.firstStepStatusMessage = firstStepStatusMessage;
    }

    public String getSecondStepStatus() {
        return secondStepStatus;
    }

    public void setSecondStepStatus(String secondStepStatus) {
        this.secondStepStatus = secondStepStatus;
    }

    public String getSecondStepStatusCode() {
        return secondStepStatusCode;
    }

    public void setSecondStepStatusCode(String secondStepStatusCode) {
        this.secondStepStatusCode = secondStepStatusCode;
    }

    public String getSecondStepStatusMessage() {
        return secondStepStatusMessage;
    }

    public void setSecondStepStatusMessage(String secondStepStatusMessage) {
        this.secondStepStatusMessage = secondStepStatusMessage;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    public LocalDateTime getNextRetryTime() {
        return nextRetryTime;
    }

    public void setNextRetryTime(LocalDateTime nextRetryTime) {
        this.nextRetryTime = nextRetryTime;
    }
}
