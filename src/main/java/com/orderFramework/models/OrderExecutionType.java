package com.orderFramework.models;

public enum OrderExecutionType {

    SINGLE_STEP, TWO_STEP;

}
