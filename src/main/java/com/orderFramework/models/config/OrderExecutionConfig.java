package com.orderFramework.models.config;

public class OrderExecutionConfig {

    private int firstStepCheckStatusPendingMaxRetry;

    private int secondStepCheckStatusPendingMaxRetry;

    private int firstStepMaxRetry;
    private int secondStepMaxRetry;

    public int getFirstStepCheckStatusPendingMaxRetry() {
        return firstStepCheckStatusPendingMaxRetry;
    }

    public void setFirstStepCheckStatusPendingMaxRetry(int firstStepCheckStatusPendingMaxRetry) {
        this.firstStepCheckStatusPendingMaxRetry = firstStepCheckStatusPendingMaxRetry;
    }

    public int getSecondStepCheckStatusPendingMaxRetry() {
        return secondStepCheckStatusPendingMaxRetry;
    }

    public void setSecondStepCheckStatusPendingMaxRetry(int secondStepCheckStatusPendingMaxRetry) {
        this.secondStepCheckStatusPendingMaxRetry = secondStepCheckStatusPendingMaxRetry;
    }

    public int getFirstStepMaxRetry() {
        return firstStepMaxRetry;
    }

    public void setFirstStepMaxRetry(int firstStepMaxRetry) {
        this.firstStepMaxRetry = firstStepMaxRetry;
    }

    public int getSecondStepMaxRetry() {
        return secondStepMaxRetry;
    }

    public void setSecondStepMaxRetry(int secondStepMaxRetry) {
        this.secondStepMaxRetry = secondStepMaxRetry;
    }
}
