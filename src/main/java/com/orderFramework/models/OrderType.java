package com.orderFramework.models;

public enum  OrderType {

    WALLET_BANK_IMPS, WALLET_BANK_UPI, PG_BANK_IMPS, WALLET_SINGLE, PG_SINGLE;

}
