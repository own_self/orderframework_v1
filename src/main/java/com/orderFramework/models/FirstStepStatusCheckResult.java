package com.orderFramework.models;


public class FirstStepStatusCheckResult {
    private SingleStepResponse singleStepResponse;
    private boolean isProceed;

    public SingleStepResponse getSingleStepResponse() {
        return singleStepResponse;
    }

    public void setSingleStepResponse(SingleStepResponse singleStepResponse) {
        this.singleStepResponse = singleStepResponse;
    }

    public boolean isProceed() {
        return isProceed;
    }

    public void setProceed(boolean proceed) {
        isProceed = proceed;
    }
}
