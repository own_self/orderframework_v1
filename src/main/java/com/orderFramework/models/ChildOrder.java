package com.orderFramework.models;

public class ChildOrder {

    private int paytmOrderId;
    private String uniqueReferenceId;
    private SingleStepStatus status;

    public int getPaytmOrderId() {
        return paytmOrderId;
    }

    public void setPaytmOrderId(int paytmOrderId) {
        this.paytmOrderId = paytmOrderId;
    }

    public String getUniqueReferenceId() {
        return uniqueReferenceId;
    }

    public void setUniqueReferenceId(String uniqueReferenceId) {
        this.uniqueReferenceId = uniqueReferenceId;
    }

    public SingleStepStatus getStatus() {
        return status;
    }

    public void setStatus(SingleStepStatus status) {
        this.status = status;
    }

}
