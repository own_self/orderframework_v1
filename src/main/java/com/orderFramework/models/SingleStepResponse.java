package com.orderFramework.models;

public class SingleStepResponse {

    private int paytmOrderId;
    private int httpStatus;
    private SingleStepStatus singleStepStatus;
    private String statusCode;
    private String statusMessage;
    private String referenceId;
    private boolean isTimeout;
    private String rawResponseBody;

    public int getPaytmOrderId() {
        return paytmOrderId;
    }

    public void setPaytmOrderId(int paytmOrderId) {
        this.paytmOrderId = paytmOrderId;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public SingleStepStatus getSingleStepStatus() {
        return singleStepStatus;
    }

    public void setSingleStepStatus(SingleStepStatus singleStepStatus) {
        this.singleStepStatus = singleStepStatus;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public boolean isTimeout() {
        return isTimeout;
    }

    public void setTimeout(boolean timeout) {
        isTimeout = timeout;
    }

    public String getRawResponseBody() {
        return rawResponseBody;
    }

    public void setRawResponseBody(String rawResponseBody) {
        this.rawResponseBody = rawResponseBody;
    }
}
