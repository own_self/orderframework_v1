package com.orderFramework.models;

import com.orderFramework.service.OrderExecutor;

import java.util.HashMap;
import java.util.Map;

public class OrderExecutionContext {

    private Order order;
    private OrderExecutionType orderExecutionType;
    private OrderBusinessContext orderBusinessContext;
    private SingleStepResponse firstStepResponse;
    private SingleStepResponse secondStepResponse;
    private OrderExecutor orderExecutor;
    private ChildOrder firstStepPendingChild;
    private ChildOrder secondStepPendingChild;
    private Object meta;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public OrderExecutionType getOrderExecutionType() {
        return orderExecutionType;
    }

    public void setOrderExecutionType(OrderExecutionType orderExecutionType) {
        this.orderExecutionType = orderExecutionType;
    }

    public OrderBusinessContext getOrderBusinessContext() {
        return orderBusinessContext;
    }

    public void setOrderBusinessContext(OrderBusinessContext orderBusinessContext) {
        this.orderBusinessContext = orderBusinessContext;
    }

    public SingleStepResponse getFirstStepResponse() {
        return firstStepResponse;
    }

    public void setFirstStepResponse(SingleStepResponse firstStepResponse) {
        this.firstStepResponse = firstStepResponse;
    }

    public SingleStepResponse getSecondStepResponse() {
        return secondStepResponse;
    }

    public void setSecondStepResponse(SingleStepResponse secondStepResponse) {
        this.secondStepResponse = secondStepResponse;
    }

    public OrderExecutor getOrderExecutor() {
        return orderExecutor;
    }

    public void setOrderExecutor(OrderExecutor orderExecutor) {
        this.orderExecutor = orderExecutor;
    }

    public ChildOrder getFirstStepPendingChild() {
        return firstStepPendingChild;
    }

    public void setFirstStepPendingChild(ChildOrder firstStepPendingChild) {
        this.firstStepPendingChild = firstStepPendingChild;
    }

    public ChildOrder getSecondStepPendingChild() {
        return secondStepPendingChild;
    }

    public void setSecondStepPendingChild(ChildOrder secondStepPendingChild) {
        this.secondStepPendingChild = secondStepPendingChild;
    }

    public Object getMeta() {
        return meta;
    }

    public void setMeta(Object meta) {
        this.meta = meta;
    }

}