package com.orderFramework.models;

public class SecondStepExecutionResult {

    private boolean isProceed;
    private boolean isTerminal;

    public boolean isProceed() {
        return isProceed;
    }

    public void setProceed(boolean proceed) {
        isProceed = proceed;
    }

    public boolean isTerminal() {
        return isTerminal;
    }

    public void setTerminal(boolean terminal) {
        isTerminal = terminal;
    }
}
