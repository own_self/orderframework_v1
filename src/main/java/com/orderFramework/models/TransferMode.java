package com.orderFramework.models;

public enum  TransferMode {

    IMPS, NEFT, RTGS, UPI;

}
