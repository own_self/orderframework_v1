package com.orderFramework.models;

public enum  RefundStatus {

    SUCCESS, FAILED, PENDING;

}
