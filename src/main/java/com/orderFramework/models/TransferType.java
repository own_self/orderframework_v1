package com.orderFramework.models;

public enum TransferType {

    WALLET_WALLET, WALLET_BANK, WALLET_ADVANCED_ACCOUNT, WALLET_LOYALTY_POINTS, PG_BANK;

}
