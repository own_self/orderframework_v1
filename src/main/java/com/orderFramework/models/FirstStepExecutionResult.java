package com.orderFramework.models;

public class FirstStepExecutionResult {

    private boolean isProceed;

    public boolean isProceed() {
        return isProceed;
    }

    public void setProceed(boolean proceed) {
        isProceed = proceed;
    }
}
