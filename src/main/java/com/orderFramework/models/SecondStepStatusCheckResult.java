package com.orderFramework.models;

public class SecondStepStatusCheckResult {

    private boolean isProceed;

    public boolean isProceed() {
        return isProceed;
    }

    public void setProceed(boolean proceed) {
        isProceed = proceed;
    }
}
